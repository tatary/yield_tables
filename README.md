# README #

* Yield tables 
* For type II supernovae and hypernovae we use the yields by Nomoto et al. 2013 
* For AGB stars, we combine yields by Campbell & Lattanzio 2008, Karakas 2010, Gil-Pons et al. 2013, and Doherty et al. 2013
* For type Ia supernovae, we employ yield by Seitenzahl et al. 2013 

### The structure of the talbes ###
* Each table contains, from left to right, time (in yr), integrated mass of ejecta, integrated newly created mass of H, He, C, N, O, Ne, Mg,
Si, S, Ca, and Fe in the ejecta, and integrated energy (in 1e51 ergs). 14 columns in total. 
* The structure of the SNIa table is different because SNIa yields are time independent. The first column contains the metallicity (M\_Z/M\_star), then then mass in ejcta, H, He, ...

### Tables ###

* SN\_HY???\_Z0.\*.dat contains data for core-collapse SNe. "HY???" indicates the fraction of hypernovae, for example "HY005" indicates that 5% of stars above 20 Msun exploed as hypernovae. 
* The number in "Z0.\*" indicate the metallicity of the stellar population; "Z0.02" means M\_Z/M\_star is 0.02. 
* The AGB tables contain the energy of stellar winds (in 1e51 ergs) in the 14th column in spite of the fact that stellar winds are completely irrelevant to the AGB stars.   
* winds\_v04\_Z0.\*.dat cnontains data for cumulative energy and momentum of stellar winds for the Genova v04 model. The structure is, time (yr), Energy (1e51 erg), momentum (g cm/s). 

### Contacts ###

* If you have any questions, please let me know (t.t.okamoto AT gmail.com)


### modified talbes ###

* In the original Nomoto et al. model, only stars of mass between 13-40 Msun explode as core-collapse SNe. In this modified SN tables, I assume stars of mass between 8-40 Msun die as CC SNe. 
